package com.niksic.uddbackend.lucene.model;

public enum SearchType {

    regular,
    fuzzy,
    phrase,
    range,
    prefix

}
