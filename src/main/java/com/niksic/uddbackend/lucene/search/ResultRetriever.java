package com.niksic.uddbackend.lucene.search;

import java.util.ArrayList;
import java.util.List;

import com.niksic.uddbackend.entities.EBook;
import com.niksic.uddbackend.lucene.indexing.handlers.DocumentHandler;
import com.niksic.uddbackend.lucene.indexing.handlers.PDFHandler;
import com.niksic.uddbackend.lucene.model.IndexUnit;
import com.niksic.uddbackend.lucene.model.RequiredHighlight;
import com.niksic.uddbackend.services.EBookService;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.highlight.HighlightBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.SearchResultMapper;
import org.springframework.data.elasticsearch.core.aggregation.AggregatedPage;
import org.springframework.data.elasticsearch.core.aggregation.impl.AggregatedPageImpl;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.stereotype.Service;

@Service
public class ResultRetriever {

    private ElasticsearchTemplate template;

    @Autowired
    private EBookService eBookService;

    @Autowired
    public ResultRetriever(ElasticsearchTemplate template){
        this.template = template;
    }

    public List<EBook> getResults(org.elasticsearch.index.query.QueryBuilder query,
                                  List<RequiredHighlight> requiredHighlights) {
        if (query == null) {
            return null;
        }

        List<EBook> results = new ArrayList<EBook>();

        SearchQuery searchQuery = new NativeSearchQueryBuilder()
                .withQuery(query)
                .build();

        List<IndexUnit> indexUnits = template.queryForList(searchQuery, IndexUnit.class);

        for (IndexUnit indexUnit : indexUnits) {
            try{
                if(eBookService.findOne(indexUnit.getBookId()) != null){
                    shouldReturnHighlightedFieldsForGivenQueryAndFields(query, indexUnit.getBookId());
                }
            } catch (Exception e){
                System.err.println("ne zahteva highlighter");
            }
            if(eBookService.findOne(indexUnit.getBookId()) != null){
                results.add(eBookService.findOne(indexUnit.getBookId()));
            }
        }
        return results;
    }

    protected DocumentHandler getHandler(String fileName){
        if(fileName.endsWith(".pdf")){
            return new PDFHandler();
        }
        return null;
    }

    public void shouldReturnHighlightedFieldsForGivenQueryAndFields(org.elasticsearch.index.query.QueryBuilder query, Long id) {

        final List<HighlightBuilder.Field> message = new ArrayList<HighlightBuilder.Field>();
        message.add(new HighlightBuilder.Field("text"));
        SearchQuery searchQuery = new NativeSearchQueryBuilder()
                .withQuery(query)
                .withHighlightFields(message.toArray(new HighlightBuilder.Field[message.size()]))
                .build();

        SearchResultMapper searchResultMapper = new SearchResultMapper() {
            public <T> AggregatedPage<T> mapResults(SearchResponse response, Class<T> clazz, Pageable pageable) {
                List<IndexUnit> chunk = new ArrayList<IndexUnit>();
                for (SearchHit searchHit : response.getHits()) {
                    String [] idStrings = searchHit.getId().split("static");
                    String [] bookFilename = eBookService.findOne(id).getFileName().split("static");
                    System.out.println(idStrings[idStrings.length-1]);
                    System.out.println(bookFilename[bookFilename.length-1]);
                    if(!idStrings[idStrings.length-1].equals(bookFilename[bookFilename.length-1])){
                        continue;
                    }
                    System.out.println("SEARCH HIT " + searchHit.getId());
                    System.out.println("FILE NAME " + eBookService.findOne(id).getFileName());
                    if (response.getHits().getHits().length <= 0) {
                        return null;
                    }
                    IndexUnit indexUnit = new IndexUnit();
                    int i = 0;
                    String highlight = "";
                    for(Text highlightField : searchHit.getHighlightFields().get("text").fragments()){
                        //indexUnit.setTitle(searchHit.getHighlightFields().get("text").fragments()[0].toString());

                        highlight = highlight + " " + highlightField;
                        i++;

                    }
                    EBook eBook = eBookService.findOne(id);
                    highlight = highlight.replace("<em>","<b class=\"highlight\">");
                    highlight = highlight.replace("</em>","</b>");
                    eBook.setHighlight(highlight);

                    System.err.println("HIGHLIGHT: " + highlight);
                    indexUnit.setTitle(searchHit.getHighlightFields().get("text").fragments()[0].toString());
                    chunk.add(indexUnit);

                    if (chunk.size() > 0) {
                        return new AggregatedPageImpl<T>((List<T>) chunk);
                    }
                    return null;
                }
                return  null;
            }
        };
        Page<IndexUnit> sampleEntities = template.queryForPage(searchQuery, IndexUnit.class, searchResultMapper);
    }
}
