package com.niksic.uddbackend.lucene.indexing.handlers;

import com.niksic.uddbackend.lucene.model.IndexUnit;

import java.io.File;

/**
 * Created by Bakir Niksic on 3/14/2018.
 */
public abstract class DocumentHandler {
    /**
     * Od prosledjene datoteke se konstruise Lucene Document
     *
     * @param file
     *            datoteka u kojoj se nalaze informacije
     * @return Lucene Document
     */
    public abstract IndexUnit getIndexUnit(File file);
    public abstract String getText(File file);

}
