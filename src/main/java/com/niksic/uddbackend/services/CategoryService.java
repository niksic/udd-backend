package com.niksic.uddbackend.services;

import com.niksic.uddbackend.entities.Category;
import com.niksic.uddbackend.entities.EBook;
import com.niksic.uddbackend.repositories.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Bakir Niksic on 2/21/2018.
 */
@Service
public class CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private EBookService eBookService;

    public List<Category> findAll(){
        return categoryRepository.findAll();
    }

    public Category findOne(Long id){
        return categoryRepository.findOne(id);
    }

    public Category save(Category category){
        return categoryRepository.save(category);
    }

    public void delete(Long id){
        for(EBook eBook : findOne(id).geteBooks()){
            eBookService.delete(eBook.getId());
        }
        categoryRepository.delete(id);
    }

    public boolean checkBookStatus (Long categoryId, Long bookId){
        for(EBook eBook : findOne(categoryId).geteBooks()){
            if(eBook.getId() == bookId){
                return true;
            }
        }
        return false;
    }

    public Category findByName(String name){
        return categoryRepository.findByName(name).get(0);
    }

    public boolean doesCategoryExist(String name){
        if(categoryRepository.findByName(name).size() == 0){
            return false;
        }
        return true;
    }

}
