package com.niksic.uddbackend.services;

import com.niksic.uddbackend.entities.Category;
import com.niksic.uddbackend.utills.ChangePasswordModel;
import com.niksic.uddbackend.utills.LoginModel;
import com.niksic.uddbackend.entities.User;
import com.niksic.uddbackend.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sun.rmi.runtime.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bakir Niksic on 2/21/2018.
 */
@Service
public class UserService {

    public static String roleAdministrator = "ADMINISTRATOR";
    public static String roleSubscriber = "SUBSCRIBER";

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CategoryService categoryService;

    public List<User> findAll(){
        return userRepository.findAll();
    }

    public User findOne(Long id){
        return userRepository.findOne(id);
    }

    public User save(User user){
        return userRepository.save(user);
    }

    public void delete(Long id){
        userRepository.delete(id);
    }

    public List<User> findByUsernameAndPassword(String username, String password){
        return userRepository.findByUsernameAndPassword(username,password);
    }

    public List<User> findByUsername(String username){
        return userRepository.findByUsername(username);
    }

    public LoginModel setLoginModel(LoginModel loginModel){
        List<User> users = findByUsernameAndPassword(loginModel.getUsername(),loginModel.getUserPassword());
        LoginModel retVal = new LoginModel();
        if(users.size() == 1){
            retVal = new LoginModel(users.get(0));
            retVal.setStatus(LoginModel.VALID_STATUS);
            return retVal;
        }else{
            retVal.setStatus(LoginModel.INVALID_STATUS);
            return retVal;
        }
    }

    public LoginModel editProfile(LoginModel loginModel){
        List<User> users = findByUsername(loginModel.getUsername());
        Category category = categoryService.findByName(loginModel.getCategory());
        LoginModel retVal = new LoginModel();
        User user = findOne(loginModel.getId());
        if(user.getUsername().equals(loginModel.getUsername())){
            System.err.println("Changing all but username!");
            user.setFirstname(loginModel.getFirstname());
            user.setLastName(loginModel.getLastName());
            user.setType(loginModel.getType());
            user.setCategory(category);
            user = save(user);
            retVal = new LoginModel(user);
            retVal.setStatus(LoginModel.VALID_STATUS);
            return retVal;
        }else{
            if(users.size() == 0){
                System.err.println("Changing username!");
                user.setFirstname(loginModel.getFirstname());
                user.setLastName(loginModel.getLastName());
                user.setUsername(loginModel.getUsername());
                user.setCategory(category);
                user.setType(loginModel.getType());
                user = save(user);
                retVal = new LoginModel(user);
                retVal.setStatus(LoginModel.VALID_STATUS);
                return  retVal;
            }
        }
        retVal.setStatus(LoginModel.INVALID_STATUS);
        return retVal;
    }

    public LoginModel changePassword(ChangePasswordModel changePasswordModel, Long id){
        LoginModel retVal = new LoginModel();
        User user = findOne(id);
        if(!user.getUserPassword().equals(changePasswordModel.getCurrentPassword())
                || !changePasswordModel.getNewPassword().equals(changePasswordModel.getRepeatPassword())
                || changePasswordModel.getNewPassword().trim().equals("")
                || changePasswordModel.getRepeatPassword().trim().equals("")){
            retVal.setStatus(LoginModel.INVALID_STATUS);
            System.err.println("PASSWORD IS NOT CHANGED!" + user.getUserPassword());
            return retVal;
        }
        user.setUserPassword(changePasswordModel.getNewPassword());
        user = save(user);
        retVal = new LoginModel(user);
        retVal.setStatus(LoginModel.VALID_STATUS);
        System.err.println("PASSWORD IS CHANGED!" + user.getUserPassword());
        return retVal;
    }

    public List<LoginModel> getAllLoginModel() {
        List<LoginModel> retVal = new ArrayList<>();
        List<User> users = findAll();
        for (User u : users){
            LoginModel loginModel = new LoginModel(u);
            retVal.add(loginModel);
        }
        return retVal;
    }

    public LoginModel addUser(LoginModel loginModel) {
        Category category = categoryService.findByName(loginModel.getCategory());
        List<User> users = findByUsername(loginModel.getUsername());
        if(users.size() > 0){
            return null;
        }
        User user = new User();
        user.setFirstname(loginModel.getFirstname());
        user.setLastName(loginModel.getLastName());
        user.setUsername(loginModel.getUsername());
        user.setUserPassword(loginModel.getUserPassword());
        user.setType(loginModel.getType());
        user.setCategory(category);
        user = save(user);
        return new LoginModel(user);
    }
}
