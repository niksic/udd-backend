package com.niksic.uddbackend.services;

import com.niksic.uddbackend.controllers.InitController;
import com.niksic.uddbackend.entities.EBook;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by Bakir Niksic on 2/28/2018.
 */
@Service
public class UploadService {

    public void getMetaDataFromPDF(String pdfPath, EBook eBook){
        try{
            File file = new File(pdfPath);
            PDDocument document = PDDocument.load(file);
            PDDocumentInformation pdd = document.getDocumentInformation();

            eBook.setAuthor(pdd.getAuthor());
            eBook.setTitle(pdd.getTitle());
            eBook.setKeywords(pdd.getKeywords());
            eBook.setMime("application/pdf");
            document.close();

        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    public EBook saveFile(MultipartFile file){
        String fileName = null;
        try{
            if (! file.isEmpty()) {
                byte[] bytes = file.getBytes();
                Path path = Paths.get(InitController.abspluteResourceFile + File.separator + file.getOriginalFilename());
                Files.write(path, bytes);
                fileName = path.toString();
            }
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        EBook eBook = new EBook();
        getMetaDataFromPDF(fileName, eBook);
        eBook.setFileName(fileName);
        return eBook;
    }
}
