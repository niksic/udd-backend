package com.niksic.uddbackend.services;

import com.niksic.uddbackend.entities.EBook;
import com.niksic.uddbackend.lucene.indexing.Indexer;
import com.niksic.uddbackend.lucene.model.IndexUnit;
import com.niksic.uddbackend.repositories.EBookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.List;

/**
 * Created by Bakir Niksic on 2/21/2018.
 */
@Service
public class EBookService {

    @Autowired
    private EBookRepository eBookRepository;

    @Autowired
    private Indexer indexer;

    public List<EBook> findAll(){
        return eBookRepository.findAll();
    }

    public EBook findOne(Long id){
        return eBookRepository.findOne(id);
    }

    public EBook save(EBook eBook){
        EBook savedEbook = eBookRepository.save(eBook);
        if(eBook.getFileName()!= null){
            IndexUnit indexUnit = indexer.getHandler(savedEbook.getFileName()).getIndexUnit(new File(savedEbook.getFileName()));
            indexUnit.setTitle(savedEbook.getTitle());
            indexUnit.setKeywords(savedEbook.getKeywords());
            indexUnit.setAuthor(savedEbook.getAuthor());
            indexUnit.setLanguage(savedEbook.getLanguage().getName());
            indexUnit.setBookId(savedEbook.getId());
            indexUnit.convertToLat();
            indexer.add(indexUnit);
        }
        return savedEbook;
    }

    public void delete(Long id){
        indexer.delete(findOne(id).getFileName());
        eBookRepository.delete(id);

    }

    public EBook updateEBook(EBook eBook) {
        EBook retVal = findOne(eBook.getId());
        retVal = retVal.update(eBook);
        return save(retVal);
    }
}
