package com.niksic.uddbackend.services;

import com.niksic.uddbackend.entities.Language;
import com.niksic.uddbackend.repositories.LanguageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Bakir Niksic on 2/21/2018.
 */
@Service
public class LanguageService {

    @Autowired
    private LanguageRepository languageRepository;

    public List<Language> findAll(){
        return languageRepository.findAll();
    }

    public Language findOne(Long id){
        return languageRepository.findOne(id);
    }

    public Language save(Language language){
        return languageRepository.save(language);
    }

    public void delete(Long id){
        languageRepository.delete(id);
    }
}
