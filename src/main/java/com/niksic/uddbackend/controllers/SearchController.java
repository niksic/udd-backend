package com.niksic.uddbackend.controllers;

import com.niksic.uddbackend.entities.EBook;
import com.niksic.uddbackend.lucene.model.*;
import com.niksic.uddbackend.lucene.search.QueryBuilder;
import com.niksic.uddbackend.lucene.search.ResultRetriever;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bakir Niksic on 3/14/2018.
 */
@RestController
@RequestMapping("/api/search")
public class SearchController {

    @Autowired
    private ResultRetriever resultRetriever;

    @CrossOrigin(origins = "*")
    @PostMapping(value="/queryParser", consumes="application/json")
    public ResponseEntity<List<EBook>> search(@RequestBody SimpleQuery simpleQuery) throws Exception {
        org.elasticsearch.index.query.QueryBuilder query= QueryBuilders.queryStringQuery(simpleQuery.getValue());
        List<RequiredHighlight> rh = new ArrayList<>();
        List<EBook> results = resultRetriever.getResults(query, rh);
        return new ResponseEntity<>(results, HttpStatus.OK);
    }

    @CrossOrigin(origins = "*")
    @PostMapping(value="/term", consumes="application/json")
    public ResponseEntity<List<EBook>> searchTermQuery(@RequestBody SimpleQuery simpleQuery) throws Exception {
        org.elasticsearch.index.query.QueryBuilder query= QueryBuilder.buildQuery(SearchType.regular, simpleQuery.getField(), simpleQuery.getValue());
        List<RequiredHighlight> rh = new ArrayList<RequiredHighlight>();
        rh.add(new RequiredHighlight(simpleQuery.getField(), simpleQuery.getValue()));
        List<EBook> results = resultRetriever.getResults(query, rh);
        return new ResponseEntity<List<EBook>>(results, HttpStatus.OK);
    }

    @CrossOrigin(origins = "*")
    @PostMapping(value="/fuzzy", consumes="application/json")
    public ResponseEntity<List<EBook>> searchFuzzy(@RequestBody SimpleQuery simpleQuery) throws Exception {
        org.elasticsearch.index.query.QueryBuilder query= QueryBuilder.buildQuery(SearchType.fuzzy, simpleQuery.getField(), simpleQuery.getValue());
        List<RequiredHighlight> rh = new ArrayList<RequiredHighlight>();
        rh.add(new RequiredHighlight(simpleQuery.getField(), simpleQuery.getValue()));
        List<EBook> results = resultRetriever.getResults(query, rh);
        return new ResponseEntity<List<EBook>>(results, HttpStatus.OK);
    }

    @CrossOrigin(origins = "*")
    @PostMapping(value="/phrase", consumes="application/json")
    public ResponseEntity<List<EBook>> searchPhrase(@RequestBody SimpleQuery simpleQuery) throws Exception {
        org.elasticsearch.index.query.QueryBuilder query= QueryBuilder.buildQuery(SearchType.phrase, simpleQuery.getField(), simpleQuery.getValue());
        List<RequiredHighlight> rh = new ArrayList<RequiredHighlight>();
        rh.add(new RequiredHighlight(simpleQuery.getField(), simpleQuery.getValue()));
        List<EBook> results = resultRetriever.getResults(query, rh);
        return new ResponseEntity<List<EBook>>(results, HttpStatus.OK);
    }

    @CrossOrigin(origins = "*")
    @PostMapping(value="/boolean", consumes="application/json")
    public ResponseEntity<List<EBook>> searchBoolean(@RequestBody AdvancedQuery advancedQuery) throws Exception {
        org.elasticsearch.index.query.QueryBuilder query1 = QueryBuilder.buildQuery(SearchType.regular, advancedQuery.getField1(), advancedQuery.getValue1());
        org.elasticsearch.index.query.QueryBuilder query2 = QueryBuilder.buildQuery(SearchType.regular, advancedQuery.getField2(), advancedQuery.getValue2());

        BoolQueryBuilder builder = QueryBuilders.boolQuery();
        if(advancedQuery.getOperation().equalsIgnoreCase("AND")){
            builder.must(query1);
            builder.must(query2);
        }else if(advancedQuery.getOperation().equalsIgnoreCase("OR")){
            builder.should(query1);
            builder.should(query2);
        }else if(advancedQuery.getOperation().equalsIgnoreCase("NOT")){
            builder.must(query1);
            builder.mustNot(query2);
        }

        List<RequiredHighlight> rh = new ArrayList<RequiredHighlight>();
        rh.add(new RequiredHighlight(advancedQuery.getField1(), advancedQuery.getValue1()));
        rh.add(new RequiredHighlight(advancedQuery.getField2(), advancedQuery.getValue2()));
        List<EBook> results = resultRetriever.getResults(builder, rh);
        return new ResponseEntity<List<EBook>>(results, HttpStatus.OK);
    }
}
