package com.niksic.uddbackend.controllers;

import com.niksic.uddbackend.entities.EBook;
import com.niksic.uddbackend.entities.Language;
import com.niksic.uddbackend.services.LanguageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Bakir Niksic on 2/27/2018.
 */
@RestController
@RequestMapping("/api/languages")
public class LanguageController {

    @Autowired
    private LanguageService languageService;

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<List<Language>> getAll(){
        System.err.println("GET ALL LANGUAGES!");
        return new ResponseEntity<List<Language>>(languageService.findAll(), HttpStatus.OK);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Language> getOne(@PathVariable("id") Long id){
        System.err.println("GET LANGUAGE!");
        Language language = languageService.findOne(id);
        if(language == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(language, HttpStatus.OK);
    }


}
