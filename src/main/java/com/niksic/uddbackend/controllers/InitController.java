package com.niksic.uddbackend.controllers;

import com.niksic.uddbackend.entities.Category;
import com.niksic.uddbackend.entities.EBook;
import com.niksic.uddbackend.entities.Language;
import com.niksic.uddbackend.entities.User;
import com.niksic.uddbackend.services.CategoryService;
import com.niksic.uddbackend.services.EBookService;
import com.niksic.uddbackend.services.LanguageService;
import com.niksic.uddbackend.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Controller;

import java.io.File;

/**
 * Created by Bakir Niksic on 2/22/2018.
 */
@Controller
public class InitController implements CommandLineRunner {

    private static final String usersPath ="./src/main/resources/static";
    public static String abspluteResourceFile;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private EBookService eBookService;

    @Autowired
    private LanguageService languageService;

    @Autowired
    private UserService userService;

    @Override
    public void run(String... args) throws Exception {
        File file = new File(usersPath);
        System.out.println("Absolute resource file is: " + file.getAbsolutePath());
        abspluteResourceFile = file.getAbsolutePath();

        Language languageSerbian = new Language("Serbian");
        languageSerbian = languageService.save(languageSerbian);

        Language languageEnglish = new Language("English");
        languageEnglish = languageService.save(languageEnglish);

        Category categoryBiography = new Category("Biography");
        categoryBiography = categoryService.save(categoryBiography);

        Category categoryComedy = new Category("Comedy");
        categoryComedy = categoryService.save(categoryComedy);

        Category categoryDrama = new Category("Drama");
        categoryDrama = categoryService.save(categoryDrama);

        Category categoryFantastic = new Category("Fantastic");
        categoryFantastic = categoryService.save(categoryFantastic);

        Category categoryAll = new Category("every");
        categoryAll = categoryService.save(categoryAll);

        User userAdmin = new User("Jovan","Jovanovic","jovan","jovan",UserService.roleAdministrator);
        User userSubscriber = new User("Stevan","Stevanovic","stevan","stevan",UserService.roleSubscriber);
        User userSubscriber2 = new User("Pavle","Pavlovic","pavle","pavle",UserService.roleSubscriber);

        userAdmin.setCategory(null);
        userSubscriber.setCategory(categoryDrama);
        userSubscriber2.setCategory(null);

        userAdmin = userService.save(userAdmin);
        userSubscriber = userService.save(userSubscriber);
        userSubscriber2 = userService.save(userSubscriber2);

        EBook book1 = new EBook("Koridor do kraja 2010","Jovan Jovanovic","koridor, put",1925,"E:\\UDD\\udd-backend\\udd-backend\\.\\src\\main\\resources\\static\\Koridor 10 do kraja 2010.pdf","MIME");
        EBook book2 = new EBook("Upravljanje digitalnim dokumentima","Bakir Niksic","dokument, udd",1966,"E:\\UDD\\udd-backend\\udd-backend\\.\\src\\main\\resources\\static\\Upravljanje digitalnim dokumentima.pdf","MIME2");
        EBook book3 = new EBook("Specifikacija projekta","Dragan Ivanovic","projekat, udd",1988,"E:\\UDD\\udd-backend\\udd-backend\\.\\src\\main\\resources\\static\\eBookRepository.pdf","MIME3");
        EBook book4 = new EBook("Vakcina H1N1","Bosko Boskovic","vakcina, h1n1",2000,"E:\\UDD\\udd-backend\\udd-backend\\.\\src\\main\\resources\\static\\Vakcina H1N1.pdf","MIME4");

        book1.setCategory(categoryComedy);
        book1.setLanguage(languageSerbian);
        book1 = eBookService.save(book1);

        book2.setCategory(categoryFantastic);
        book2.setLanguage(languageEnglish);
        book2 = eBookService.save(book2);

        book3.setCategory(categoryDrama);
        book3.setLanguage(languageSerbian);
        book3 = eBookService.save(book3);

        book4.setCategory(categoryBiography);
        book4.setLanguage(languageSerbian);
        book4 = eBookService.save(book4);




    }
}
