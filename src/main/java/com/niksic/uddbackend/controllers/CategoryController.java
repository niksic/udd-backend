package com.niksic.uddbackend.controllers;

import com.niksic.uddbackend.entities.Category;
import com.niksic.uddbackend.entities.EBook;
import com.niksic.uddbackend.services.CategoryService;
import com.niksic.uddbackend.services.EBookService;
import com.niksic.uddbackend.utills.LoginModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Bakir Niksic on 2/22/2018.
 */
@RestController
@RequestMapping("/api/category")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private EBookService eBookService;

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<List<Category>> getAll(){
        return new ResponseEntity<List<Category>>(categoryService.findAll(), HttpStatus.OK);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Category> getOne(@PathVariable("id") Long id){
        Category category = categoryService.findOne(id);
        if(category == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Category>(category, HttpStatus.OK);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/{id}/ebooks", method = RequestMethod.GET)
    public ResponseEntity<List<EBook>> getEBooks(@PathVariable("id") Long id){
        Category category = categoryService.findOne(id);
        if(category == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<List<EBook>>(category.geteBooks(), HttpStatus.OK);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/{id}/ebooks/{bookId}", method = RequestMethod.GET)
    public ResponseEntity<EBook> getEBooks(@PathVariable("id") Long id, @PathVariable("bookId") Long bookId){
        Category category = categoryService.findOne(id);
        if(category == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        EBook eBook = eBookService.findOne(bookId);
        if(!categoryService.checkBookStatus(id,bookId)){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<EBook>(eBook, HttpStatus.OK);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseEntity<Category> add(@RequestBody Category category){
        System.err.println("ADD CATEGORY");
        if(categoryService.doesCategoryExist(category.getName())){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        category = categoryService.save(category);
        return new ResponseEntity<>(category, HttpStatus.OK);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Category> update(@PathVariable("id") Long id, @RequestBody Category category){
        System.err.println("PUT CATEGORY");
        if(categoryService.doesCategoryExist(category.getName())){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        Category updatedCategory = categoryService.findOne(id);
        updatedCategory.setName(category.getName());
        updatedCategory  = categoryService.save(updatedCategory );
        return new ResponseEntity<>(updatedCategory , HttpStatus.OK);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Category> delete(@PathVariable("id") Long id){
        System.err.println("DELETE CATEGORY");
        Category category = categoryService.findOne(id);
        if(category == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        categoryService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/{id}/ebooks", method = RequestMethod.POST)
    public ResponseEntity<EBook> addBook(@PathVariable("id") Long categoryId, @RequestBody EBook eBook){
        Category category = categoryService.findOne(categoryId);
        if(category == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        eBook = eBookService.save(eBook);
        return new ResponseEntity<EBook>(eBook, HttpStatus.OK);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/{categoryId}/ebooks/{eBookId}", method = RequestMethod.PUT)
    public ResponseEntity<EBook> updateBook(@PathVariable("categoryId") Long categoryId,
                                            @PathVariable("eBookId") Long eBookId,
                                            @RequestBody EBook eBook){
        Category category = categoryService.findOne(categoryId);
        if(category == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        EBook existingEBook = eBookService.findOne(eBookId);
        if(existingEBook == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<EBook>(eBookService.updateEBook(eBook), HttpStatus.OK);
    }

}
