package com.niksic.uddbackend.controllers;

import com.niksic.uddbackend.utills.ChangePasswordModel;
import com.niksic.uddbackend.utills.LoginModel;
import com.niksic.uddbackend.entities.User;
import com.niksic.uddbackend.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sun.rmi.runtime.Log;

import java.util.List;

/**
 * Created by Bakir Niksic on 2/24/2018.
 */
@RestController
@RequestMapping("/api/users")
public class UserController {

    @Autowired
    private UserService userService;

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<List<LoginModel>> get(){
        System.err.println("GET USERS");
        List<LoginModel> users = userService.getAllLoginModel();
        if(users == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<List<LoginModel>>(users, HttpStatus.OK);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<LoginModel> get(@PathVariable("id") Long id){
        System.err.println("GET USER");
        User user = userService.findOne(id);
        if(user == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<LoginModel>(new LoginModel(user), HttpStatus.OK);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<LoginModel> update(@PathVariable("id") Long id, @RequestBody LoginModel loginModel){
        System.err.println("PUT USER EDIT PROFILE");
        User user = userService.findOne(id);
        if(user == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<LoginModel>(userService.editProfile(loginModel), HttpStatus.OK);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/changePassword/{id}", method = RequestMethod.PUT)
    public ResponseEntity<LoginModel> updatePassword(@PathVariable("id") Long id, @RequestBody ChangePasswordModel changePasswordModel){
        System.err.println("PUT USER PW CHANGE");
        User user = userService.findOne(id);
        if(user == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<LoginModel>(userService.changePassword(changePasswordModel, id), HttpStatus.OK);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseEntity<LoginModel> add(@RequestBody LoginModel loginModel){
        System.err.println("ADD USER");
        LoginModel addedUser = userService.addUser(loginModel);
        if(addedUser == null){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<LoginModel>(addedUser, HttpStatus.OK);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<LoginModel> delete(@PathVariable("id") Long id){
        System.err.println("DELETE USER");
        User user = userService.findOne(id);
        if(user == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        userService.delete(id);
        return new ResponseEntity<>(new LoginModel(user),HttpStatus.OK);
    }
}
