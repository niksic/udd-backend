package com.niksic.uddbackend.controllers;

import com.niksic.uddbackend.entities.EBook;
import com.niksic.uddbackend.services.EBookService;
import com.niksic.uddbackend.services.UploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.awt.print.Book;
import java.util.List;

/**
 * Created by Bakir Niksic on 2/26/2018.
 */
@RestController
@RequestMapping("/api/ebooks")
public class EBooksController {

    @Autowired
    private EBookService eBookService;

    @Autowired
    private UploadService uploadService;

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<List<EBook>> getAll(){
        System.err.println("GET ALL E-BOOKS!");
        return new ResponseEntity<List<EBook>>(eBookService.findAll(), HttpStatus.OK);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<EBook> getEBooks(@PathVariable("id") Long id){
        EBook eBook = eBookService.findOne(id);
        if(eBook == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<EBook>(eBook, HttpStatus.OK);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<EBook> delete(@PathVariable("id") Long id){
        System.err.println("DELETE E-BOOK!");
        EBook eBook = eBookService.findOne(id);
        if(eBook == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        eBookService.delete(id);
        return new ResponseEntity<>(eBook, HttpStatus.OK);
    }

    @CrossOrigin(origins="*")
    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public ResponseEntity<EBook> uploadFile(@RequestBody MultipartFile file) {
        if (file == null) {
            return new ResponseEntity<EBook>(HttpStatus.BAD_REQUEST);
        }
        System.out.println(file);
        return new ResponseEntity<EBook>(uploadService.saveFile(file), HttpStatus.OK);
    }
}
