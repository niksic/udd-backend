package com.niksic.uddbackend.controllers;

import com.niksic.uddbackend.utills.LoginModel;
import com.niksic.uddbackend.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Bakir Niksic on 2/22/2018.
 */
@RestController
@RequestMapping("/api/login")
public class LoginController {

    @Autowired
    private UserService userService;

    @CrossOrigin(origins="*")
    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseEntity<LoginModel> postLogin(@RequestBody LoginModel loginModel) {
        if (loginModel == null) {
            return new ResponseEntity<LoginModel>(HttpStatus.BAD_REQUEST);
        }

        loginModel = userService.setLoginModel(loginModel);
        System.err.println("user " + loginModel.getUsername());
        return new ResponseEntity<LoginModel>(loginModel, HttpStatus.OK);
    }


}
