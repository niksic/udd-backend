package com.niksic.uddbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UddBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(UddBackendApplication.class, args);
	}
}
