package com.niksic.uddbackend.repositories;

import com.niksic.uddbackend.entities.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by Bakir Niksic on 2/21/2018.
 */
public interface CategoryRepository extends JpaRepository<Category, Long> {

    @Query("select category from Category category where category.name = :name")
    public List<Category> findByName(@Param("name") String name);
}
