package com.niksic.uddbackend.repositories;

import com.niksic.uddbackend.entities.EBook;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Bakir Niksic on 2/21/2018.
 */
public interface EBookRepository extends JpaRepository<EBook, Long> {
}
