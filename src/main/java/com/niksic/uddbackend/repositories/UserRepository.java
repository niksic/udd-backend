package com.niksic.uddbackend.repositories;

import com.niksic.uddbackend.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by Bakir Niksic on 2/21/2018.
 */
public interface UserRepository extends JpaRepository<User, Long> {

    @Query("select user from User user where user.username = :username and user.userPassword = :password")
    public List<User> findByUsernameAndPassword(@Param("username") String username, @Param("password") String password);

    @Query("select user from User user where user.username = :username")
    public List<User> findByUsername(@Param("username") String username);
}
