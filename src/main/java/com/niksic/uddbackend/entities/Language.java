package com.niksic.uddbackend.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bakir Niksic on 2/21/2018.
 */
@Entity
public class Language {

    @Id
    @GeneratedValue
    private Long id;

    private String name;

    @JsonIgnore
    @OneToMany(mappedBy = "language", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<EBook> eBooks = new ArrayList<>();

    public Language(){

    }

    public Language(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<EBook> geteBooks() {
        return eBooks;
    }

    public void seteBooks(List<EBook> eBooks) {
        this.eBooks = eBooks;
    }
}
