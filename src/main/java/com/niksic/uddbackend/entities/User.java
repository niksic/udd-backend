package com.niksic.uddbackend.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bakir Niksic on 2/21/2018.
 */
@Entity
public class User {

    @Id
    @GeneratedValue
    private Long id;

    private String firstname;
    private String lastName;
    private String username;
    private String userPassword;
    private String type;

    @ManyToOne(fetch = FetchType.EAGER, optional = true)
    private Category category;
//
//    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
//    private List<EBook> eBooks = new ArrayList<>();


    public User(){

    }

    public User(String firstname, String lastName, String username, String userPassword, String type) {
        this.firstname = firstname;
        this.lastName = lastName;
        this.username = username;
        this.userPassword = userPassword;
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

//    public List<EBook> geteBooks() {
//        return eBooks;
//    }
//
//    public void seteBooks(List<EBook> eBooks) {
//        this.eBooks = eBooks;
//    }
}
