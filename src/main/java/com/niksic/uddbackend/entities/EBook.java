package com.niksic.uddbackend.entities;

import javax.persistence.*;

/**
 * Created by Bakir Niksic on 2/21/2018.
 */
@Entity
public class EBook {

    @Id
    @GeneratedValue
    private Long id;

    private String title;
    private String author;
    private String keywords;
    private int publicationYear;
    private String fileName;
    private String mime;
    private String highlight;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    private Language language;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    private Category category;
//
//    @ManyToOne(fetch = FetchType.EAGER, optional = false)
//    private User user;

    public EBook(){

    }

    public EBook(String title, String author, String keywords, int publicationYear, String fileName, String mime) {
        this.title = title;
        this.author = author;
        this.keywords = keywords;
        this.publicationYear = publicationYear;
        this.fileName = fileName;
        this.mime = mime;
    }

    public EBook update(EBook eBook){
        this.title = eBook.getTitle();
        this.author = eBook.getAuthor();
        this.keywords = eBook.getKeywords();
        this.publicationYear = eBook.getPublicationYear();
        this.fileName = eBook.getFileName();
        this.mime = eBook.getMime();
        this.setLanguage(eBook.getLanguage());
        this.setCategory(eBook.getCategory());
        return this;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public int getPublicationYear() {
        return publicationYear;
    }

    public void setPublicationYear(int publicationYear) {
        this.publicationYear = publicationYear;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getMime() {
        return mime;
    }

    public void setMime(String mime) {
        this.mime = mime;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
//
//    public User getUser() {
//        return user;
//    }
//
//    public void setUser(User user) {
//        this.user = user;
//    }

    public String getHighlight() {
        return highlight;
    }

    public void setHighlight(String highlight) {
        this.highlight = highlight;
    }
}
