package com.niksic.uddbackend.utills;

import com.niksic.uddbackend.entities.User;

/**
 * Created by Bakir Niksic on 2/22/2018.
 */
public class LoginModel {

    public static String VALID_STATUS = "VALID";
    public static String INVALID_STATUS = "INVALID";

    private Long id;
    private String firstname;
    private String lastName;
    private String username;
    private String userPassword;
    private String type;
    private String status;
    private String category;

    public LoginModel() {
    }

    public LoginModel(User user) {
        this.id = user.getId();
        this.firstname = user.getFirstname();
        this.lastName = user.getLastName();
        this.username = user.getUsername();
        this.userPassword = user.getUserPassword();
        this.type = user.getType();
        if(user.getCategory() == null){
            this.category = "every";
        }else{
            this.category = user.getCategory().getName();
        }
        System.err.println("Category set " + this.category);
    }

    public LoginModel(String firstname, String lastName, String username, String userPassword, String type, String status) {
        this.firstname = firstname;
        this.lastName = lastName;
        this.username = username;
        this.userPassword = userPassword;
        this.type = type;
        this.status = status;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
