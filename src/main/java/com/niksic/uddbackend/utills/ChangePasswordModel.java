package com.niksic.uddbackend.utills;

/**
 * Created by Bakir Niksic on 2/24/2018.
 */
public class ChangePasswordModel {

    private String currentPassword;
    private String newPassword;
    private String repeatPassword;

    public ChangePasswordModel(){

    }

    public String getCurrentPassword() {
        return currentPassword;
    }

    public void setCurrentPassword(String currentPassword) {
        this.currentPassword = currentPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getRepeatPassword() {
        return repeatPassword;
    }

    public void setRepeatPassword(String repeatPassword) {
        this.repeatPassword = repeatPassword;
    }
}
